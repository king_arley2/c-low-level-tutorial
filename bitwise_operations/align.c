#include <stdio.h>

#define PAGE_SIZE 4096

int align_lower(int number, int alignment)
{
    return number & (~(alignment- 1));
}

int align_upper(int number, int alignment)
{
    return (number + (alignment- 1)) & (~(alignment- 1));
}

int is_aligned(int number, int alignment)
{
    return !(number & (alignment - 1));
}

int modulo(int number, int alignment)
{
    return number & (alignment - 1);
}

int main()
{
    printf("5000 aligned lower to PAGE_SIZE: %d\n", align_lower(5000, PAGE_SIZE));
    printf("5000 aligned upper to PAGE_SIZE: %d\n", align_upper(5000, PAGE_SIZE));

    printf("4096 aligned lower to PAGE_SIZE: %d\n", align_lower(4096, PAGE_SIZE));
    printf("4096 aligned upper to PAGE_SIZE: %d\n", align_upper(4096, PAGE_SIZE));

    printf("4097 aligned lower to PAGE_SIZE: %d\n", align_lower(4097, PAGE_SIZE));
    printf("4097 aligned upper to PAGE_SIZE: %d\n", align_upper(4097, PAGE_SIZE));

    printf("5000 modulo PAGE_SIZE: %d\n", modulo(5000, PAGE_SIZE));

    printf("8192 is aligned to PAGE_SIZE: %d\n", is_aligned(8192, PAGE_SIZE));
    printf("8300 is aligned to PAGE_SIZE: %d\n", is_aligned(8300, PAGE_SIZE));
}

