#include <stdio.h>
#include <stdint.h>

uint32_t bitPosition32(uint32_t number)
{
    uint32_t lut[32] =
    {
      0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
      31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
    };
    return lut[(number * 0x077CB531U) >> 27];
}

uint8_t bitPosition8(uint8_t number)
{
    uint8_t lut[8] = {0, 1, 2, 4, 7, 3, 6, 5};
    return lut[((number * 0x17) >> 5) & 7];
}

int main()
{
    uint8_t i;
    uint32_t j;
    for (i = 1; i != 0; i <<= 1)
    {
        printf("bit position for 0x%x is: %d\n", i, bitPosition8(i));
    }
    printf("\n");

    for (j = 1; j != 0; j <<= 1)
    {
        printf("bit position for 0x%x is: %d\n", j, bitPosition32(j));
    }
}

