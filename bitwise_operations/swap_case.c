#include <stdio.h>
#include <ctype.h>

void uc(char *buffer)
{
    int i;
    for (i = 0; buffer[i]; i++)
    {
        if (isalpha(buffer[i]))
        {
            buffer[i] &= -33;
        }
    }
}

void lc(char *buffer)
{
    int i;
    for (i = 0; buffer[i]; i++)
    {
        if (isalpha(buffer[i]))
        {
            buffer[i] |= 32;
        }
    }
}

void tc(char *buffer)
{
    int i;
    for (i = 0; buffer[i]; i++)
    {
        if (isalpha(buffer[i]))
        {
            buffer[i] ^= 32;
        }
    }
}

int main()
{
    char buffer[] = "ana are mere multe si pere putine";
    uc(buffer);
    printf("buffer: %s\n", buffer);
    lc(buffer);
    printf("buffer: %s\n", buffer);
    tc(buffer);
    printf("buffer: %s\n", buffer);
    tc(buffer);
    printf("buffer: %s\n", buffer);
}

