#include <stdio.h>

/* doesn't work for identical memory locations */
#define xor_swap(a,b) \
do \
{\
    (a) = (a) ^ (b);\
    (b) = (a) ^ (b);\
    (a) = (a) ^ (b);\
}\
while(0)

int main()
{
    int a = 5;
    int b = 10;
    printf("a = %d; b = %d \n", a, b);
    xor_swap(a, b);
    printf("a = %d; b = %d \n", a, b);
    xor_swap(a, a);
    printf("a = %d\n", a);
}

