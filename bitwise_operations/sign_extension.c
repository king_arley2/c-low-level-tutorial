#include <stdio.h>

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof(arr[0]))

int absolute_value(int number)
{
    int const mask = number >> (sizeof(int) * 8 - 1);
    return (number + mask) ^ mask;
    /* same as: return (number ^ mask) - mask; */
}

int max(int a, int b)
{
    return ((a + b) + absolute_value(a-b)) >> 1;
}

int min(int a, int b)
{
    return ((a + b) - absolute_value(a-b)) >> 1;
}

void branchless_sum()
{
    int i;
    int sum = 0;
    int target = 15;
    int numbers[] = { 10, 15, 14, 16, 30, 43 };

    /* add all the numbers < target */
    for (i = 0; i < ARRAY_SIZE(numbers); i++)
    {
        sum += ((numbers[i] - target) >> (8 * sizeof(int) - 1)) & numbers[i];
    }
    printf("sum is %d\n", sum);
}

int main()
{
    printf("%d %d\n", absolute_value(-100), absolute_value(100));
    printf("%d\n", max(20, 40));
    printf("%d\n", min(20, 40));
    branchless_sum();
}

