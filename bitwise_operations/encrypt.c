#include <stdio.h>
#include <ctype.h>
#include <string.h>

void printBuffer(char *buffer, int length)
{
    int i;
    for (i = 0; i < length; i++)
    {
        if (isprint(buffer[i]))
        {
            printf("%c", buffer[i]);
        }
        else
        {
            printf(".");
        }
    }
    printf("\n");
}

void dumpBuffer(char *buffer, int length, int nr_bytes_displayed)
{
    int i;
    int j;
    int buf_index;
    char displayBuffer[100];
    char tempBuffer[10];
    for (i = 0; i < length; i += nr_bytes_displayed)
    {
        displayBuffer[0] = '\0';
        for (j = 0; j < nr_bytes_displayed; j++)
        {
            buf_index = i + j;
            if (buf_index >= length)
            {
                break;
            }
            snprintf(tempBuffer, sizeof(displayBuffer), "%02X ", (unsigned char) buffer[buf_index]);
            strcat(displayBuffer, tempBuffer);
        }

        printf("0x%p: ", &buffer[i]);
        printf("%-*s| ", nr_bytes_displayed * 3, displayBuffer);

        for (j = 0; j < nr_bytes_displayed; j++)
        {
            buf_index = i + j;
            if (buf_index >= length)
            {
                break;
            }
            if (isprint(buffer[buf_index]))
            {
                printf("%c", buffer[buf_index]);
            }
            else
            {
                printf(".");
            }
        }

        printf("\n");
    }
    /* add another newline at the end of the dump for output clarity */
    printf("\n");
}

void crypt(char *buffer, int length, char key)
{
    int i;
    for (i = 0; i < length; i++)
    {
        buffer[i] ^= key;
    }
}

#define string_length(str) (sizeof(str) - 1)

int main()
{
    char buffer[] = "Long string to exemplify the xor encryption/decryption process";
    char key = 0xD7;
    dumpBuffer(buffer, string_length(buffer), 8);
    crypt(buffer, string_length(buffer), key);
    dumpBuffer(buffer, string_length(buffer), 8);
    crypt(buffer, string_length(buffer), key);
    dumpBuffer(buffer, string_length(buffer), 8);
}

