#include <stdio.h>

/* Counting bits set, Brian Kernighan's way */
/* Brian Kernighan's method goes through as many iterations as there are set
 * bits. So if we have a 32-bit word with only the high bit set, then it will
 * only go once through the loop. */
int bc(int number)
{
    int count;
    for (count = 0; number; count++)
    {
        /* clear the least significant bit set */
        number &= number - 1;
    }
    return count;
}

int isPowerOf2(int number)
{
    return number && !(number & (number - 1));
}

int main()
{
    printf("%d\n", bc(0x17));
    printf("%d %d\n", isPowerOf2(64), isPowerOf2(100));
}

