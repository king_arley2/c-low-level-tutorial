bits 32
extern printf
global main

section .data
    message db "Hello world!!", 10, 0

section .text

main:
    push    ebp
    mov     ebp, esp
    push    message
    call    printf
    add     esp, 4
    pop     ebp
    ret
