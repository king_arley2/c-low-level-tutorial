bits 32
extern printf
extern scanf
global main

section .data
    message db "%d", 10, 0
    fibo_index dd 0
    scanf_msg db "%d", 0

section .text

printNumber:
    push    ebp
    mov     ebp, esp

    push    dword [ebp + 8]
    push    message
    call    printf
    add     esp, 8

    pop     ebp
    ret

main:
    push    ebp
    mov     ebp, esp

    push    fibo_index
    push    scanf_msg
    call    scanf
    add     esp, 8

    xor     esi, esi
    xor     edi, edi
    inc     edi
    xor     ecx, ecx
.loop:
    cmp     ecx, [fibo_index]
    jz      .endloop
    push    ecx
    push    edi
    call    printNumber
    add     esp, 4
    pop     ecx
    mov     edx, esi
    add     edx, edi
    mov     esi, edi
    mov     edi, edx
    inc     ecx
    jmp     .loop
.endloop:

    pop     ebp
    ret
