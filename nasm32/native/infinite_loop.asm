SECTION .data
msg     db      'Hello World!', 0Ah

SECTION .text
global  _start              ; the linker needs to know the entry point

printHello:
    push    ebp             ; the caller expects that ebp won't be changed
    mov     ebp, esp        ; get a fix reference to the stack pointer

    mov     eax, 4          ; SYS_WRITE system call
    mov     ebx, 1          ; descriptor 1 is stdout
    mov     ecx, [ebp + 8]  ; the buffer is in the first parameter
    mov     edx, [ebp + 12] ; the length is in the second parameter
    int     80h             ; system call

    pop     ebp             ; restore caller's ebp

    ; pushing the address of this function on the stack will cause an
    ; infinite loop
    push    DWORD printHello
    ; ret will pop this from the stack and will jump to it, causing an
    ; infinite recursive call
    ret

_start:                     ; the entry point of the program
    push    13              ; the length of the buffer (second parameter)
    push    msg             ; the buffer (first parameter)
    call    printHello
    add     esp,    8       ; remove the 2 parameters from the stack

    mov     ebx, 0          ; return 0 status on exit - 'No Errors'
    mov     eax, 1          ; invoke SYS_EXIT (kernel opcode 1)
    int     80h
