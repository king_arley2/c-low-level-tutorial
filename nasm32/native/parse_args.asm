SECTION .data
newLine db 0xA, 0

SECTION .text
global  _start

strlen:
    push    ebp
    mov     ebp, esp

    mov    edi, [ebp+8]
    xor    ecx, ecx
.loop:
    cmp    [edi+ecx], BYTE 0
    jz     .end
    inc    ecx
    jmp    .loop
.end:
    mov    eax, ecx
    pop    ebp
    ret

printStr:
    push      ebp
    mov       ebp, esp
    push      dword [ebp + 8]
    call      strlen
    add       esp, 4

    ;         sys_write
    mov       edx, eax
    mov       eax, 4
    mov       ebx, 1
    mov       ecx, [ebp + 8]
    int       80h

    ;print    newline
    ;         sys_write
    mov       edx, 1
    mov       eax, 4
    mov       ebx, 1
    mov       ecx, newLine
    int       80h

    pop       ebp
    ret

_start:
    push    ebp
    mov     ebp, esp

    xor    ecx, ecx
.loop:
    cmp    ecx, [ebp + 4]
    jz     .end
    push    ecx
    mov    edi, [ebp + 8 + ecx * 4]

    push    edi
    call    printStr
    add     esp, 4
    pop     ecx
    inc     ecx
    jmp     .loop
.end:

    mov     ebx, 0      ; return 0 status on exit - 'No Errors'
    mov     eax, 1      ; invoke SYS_EXIT (kernel opcode 1)
    int     80h
