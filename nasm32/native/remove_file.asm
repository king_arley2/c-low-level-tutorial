SECTION .data
msg     times 100 db 0

SECTION .text
global  _start

remove_newline:
    push    ebp
    mov     ebp, esp
    xor     ecx, ecx
    mov     esi, [ebp + 8]
.loop:
    cmp     [esi + ecx], BYTE 0
    jz .endloop
    cmp     [esi + ecx], BYTE 0xA
    jnz .over
    mov     [esi + ecx], BYTE 0
    jmp     .endloop
.over:
    inc     ecx
    jmp     .loop
.endloop:
    pop     ebp
    ret

_start:
    ; sys_read
    mov     eax, 0x3
    xor     ebx, ebx
    mov     ecx, msg
    mov     edx, 100
    int     80h

    ; write as many bytes as read
    mov     edx, eax

    ; sys_write
    mov     eax, 4
    mov     ebx, 1
    mov     ecx, msg
    int     80h

    push    msg
    call    remove_newline
    add     ebp, 4

    ; sys_unlink
    mov     eax, 0xA
    mov     ebx, msg
    int     80h

    mov     ebx, 0      ; return 0 status on exit - 'No Errors'
    mov     eax, 1      ; invoke SYS_EXIT (kernel opcode 1)
    int     80h
