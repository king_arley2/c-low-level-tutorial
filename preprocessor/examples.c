#include <stdio.h>

#define square(a) a*a
#define add(a, b) a+b

int get(int param)
{
	printf("getter was called\n");
	return param;
}

int main()
{
	printf("square(10): %d\n", square(10));
	printf("square(9+1): %d\n", square(9+1));
	printf("square(10): %d\n", square(get(10)));
	printf("add(9+1): %d\n", add(9, 1));
	printf("2 * add(9+1): %d\n", 2 * add(9, 1));
	printf("add(3<<1, 4): %d\n", add(3<<1, 4));
}
