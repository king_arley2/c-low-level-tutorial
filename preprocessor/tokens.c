#include <stdio.h>

#define PAGE_SIZE 4096
#define PAGE_SIZE2 PAGE_SIZE

#define str(a) #a
#define xstr(a) str(a)

#define print_macro(identifier) printf("string: <%s> string: <%s> number: <%d>\n", #identifier, xstr(identifier), identifier)

int main()
{
	printf("%s\n", str(PAGE_SIZE));
	printf("%s\n", xstr(PAGE_SIZE));
	print_macro(PAGE_SIZE);
}

