#include <stdio.h>

#define get(class, field) class##_##field##_get()
#define get_generator(class, field) typeof(class.field) class##_##field##_get(){return class.field;}
#define set(class, field, value) class##_##field##_set(value)
#define set_generator(class, field) void class##_##field##_set(typeof(class.field) value){class.field=value;}

typedef struct _STUDENT
{
	int age;
	double income;
} STUDENT;
STUDENT ana;

get_generator(ana, age)
get_generator(ana, income)
set_generator(ana, age)
set_generator(ana, income)

int main()
{
	set(ana, age, 20);
	set(ana, income, 14.3);
	printf("age: %d income: %f\n", get(ana, age), get(ana, income));
}

