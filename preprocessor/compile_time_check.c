#include <stdio.h>
/* Force a compilation error if condition is true, but also produce a
   result (of value 0 and type size_t), so the expression can be used
   e.g. in a structure initializer (or where-ever else comma expressions
   aren't permitted). */
#define BUILD_BUG_ON_ZERO(e) (sizeof(struct { int:-!!(e); }))
#define ARRAY_SIZE(vector) (sizeof(vector) / sizeof(*(vector)))

int main()
{
	int ana[] = {1,2,3,4};
	return BUILD_BUG_ON_ZERO(ARRAY_SIZE(ana) != 4);
}

