#include <stdio.h>

/* do {...} while(0) */
/* It's the only construct in C that you can use to #define a multistatement
 * operation, put a semicolon after, and still use within an if statement. */
#define print_hello() \
do\
{\
	log("hello world");\
}\
while(0)

/* the ‘##’ token paste operator has a special meaning when placed between a
 * comma and a variable argument */
/* if the variable argument is left out when the macro is used, then the comma
 * before the ‘##’ will be deleted. */
#define log(message, ...) printf("file: %s, line: %d "message"\n", __FILE__, __LINE__, ##__VA_ARGS__)
int main()
{
	print_hello();
}

