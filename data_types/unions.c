#include <stdio.h>

#define LSB_BITFIELD
#ifdef LSB_BITFIELD
typedef union
{
	struct
	{
		int status:4;
		int unused:16;
		int configuration:12;
	};
	int raw;
} flags;

typedef struct
{
	int type;
	union
	{
		struct
		{
			int sa;
			int da;
			int checksum;
		} tcp;
		struct
		{
			int da;
			int sa;
		} udp;
	};
} net_packet;

struct
{
	char ana; //7 bytes
	long long mere; //0
	char ana2; //7
} dummy;


typedef struct
{
	char ana; //7 bytes
	struct
	{
		char bere;
		long long cere;
		char dere;
	};
	char fere;
} dummy2;

#pragma pack(push, 1)
typedef struct
{
	char ana; //7 bytes
	struct
	{
		char bere;
		long long cere;
		char dere;
	};
	char fere;
} dummy3;
#pragma pack(pop)

typedef struct _flags8
{
	char status:3;
	char unused:4;
} flags8;
#else
typedef struct _flags
{
	int configuration:12;
	int unused:16;
	int status:4;
} flags;
#endif

typedef union _arr
{
	struct
	{
		char a;
		int b;
	} ana;
	unsigned char mere[5];
} arr;

#define status_mask 0xF
#define get_status(a) ((a) & status_mask)

#define get_configuration(a) ((a) >> 20)
#define get_unused(a) (((a) >> 4) & 0xFFFF)

void f()
{
	int i;
	arr ex;
	ex.ana.a = 0x50;
	ex.ana.b = 0x12345678;
	for (i = 0; i < sizeof(ex.mere); i++)
	{
		printf("0x%x ", ex.mere[i]);
	}
}
void g()
{
	flags reg = {{0}};
	reg.status = 4;
	reg.configuration = 4;
	printf("%d\n", reg.status);
	printf("0x%x\n", reg.raw);
}

int main()
{
	net_packet pk = {0};
	if (pk.type == 1)
	{
		printf("sa: %x\n", pk.udp.sa);
	}
	else
	{
		printf("sa: %x\n", pk.tcp.sa);
	}
	printf("%zu\n", sizeof(net_packet));
	printf("%zu\n", sizeof(short));
	printf("dummy: %zu\n", sizeof(dummy));
	printf("dummy2: %zu\n", sizeof(dummy2));
	printf("dummy3: %zu\n", sizeof(dummy3));
	printf("informatie utila: %f\n", (double)sizeof(dummy3) / sizeof(dummy2) * 100);
}

