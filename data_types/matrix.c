#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STATUS_SUCCESS(rc) ((rc) == SUCCESS)
#define STATUS_CLEANUP(error) \
do\
{\
	status = error;\
	printf("function failed with error: %d %d\n", error, __LINE__);\
	goto out;\
} while(0);

#define STATUS_CLEANUP_IF(cond, error) \
if(cond)\
{\
	STATUS_CLEANUP(error);\
}

#define SUCCESS 0
#define ERROR 1
#define INVALID_PARAMETER 2
#define ERROR_NO_MEM 3

int allocMatrix(int ***matrix, int nrLines, int nrColumns)
{
	int i;
	int status = ERROR;

	STATUS_CLEANUP_IF(!matrix, INVALID_PARAMETER);
	STATUS_CLEANUP_IF(nrLines < 0 || nrColumns < 0, INVALID_PARAMETER);

	(*matrix) = malloc(nrLines * sizeof(*(*matrix)));
	STATUS_CLEANUP_IF((!(*matrix)), ERROR_NO_MEM);

	memset((*matrix), 0, nrLines * sizeof(*(*matrix)));

	for (i = 0; i < nrLines; i++)
	{
		(*matrix)[i] = malloc(nrColumns * sizeof(*(*matrix)[i]));
		STATUS_CLEANUP_IF((!(*matrix)[i]), ERROR_NO_MEM);
	}
	status = SUCCESS;
	return status;
out:
	if (matrix && (*matrix))
	{
		for (i = 0; i < nrLines; i++)
		{
			if ((*matrix)[i])
			{
				free((*matrix)[i]);
				(*matrix)[i] = NULL;
			}
		}
		free((*matrix));
		*matrix = NULL;
	}
	return status;
}

int main()
{
	int i;
	int nrLines = 10000;
	int nrColumns = 10000;
	int **matrix = NULL;
	int status;

	status = allocMatrix(&matrix, nrLines, nrColumns);
	STATUS_CLEANUP_IF((!STATUS_SUCCESS(status)), ERROR);

	matrix[1][2] = 1;
	printf("%d\n", matrix[1][2]);

out:
	if (matrix)
	{
		for (i = 0; i < nrLines; i++)
		{
			if (matrix[i])
			{
				free(matrix[i]);
			}
		}
		free(matrix);
	}
}

