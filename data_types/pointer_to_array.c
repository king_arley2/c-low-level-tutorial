#include<stdio.h>

void ana_are_mere(int (*a)[5])
{
	printf("array has: %p, %zu\n", a, sizeof(*a));
}

void ana_are_pere(int *a)
{
	printf("array has: %p, %zu\n", a, sizeof(&a));
}

int main()
{
	// Pointer to an integer
	int *p;

	// Pointer to an array of 5 integers
	int (*ptr)[5];
	int arr[5] = {1,2,3,4,5};

	// Points to 0th element of the arr.
	p = arr;

	// Points to the whole array arr.
	ptr = &arr;

	printf("p = %p, ptr = %p\n", p, ptr);
	printf("*p = %d, *ptr = %p\n", *p, *ptr);

	printf("&arr %p, &arr + 1: %p\n", &arr, &arr + 1);

	printf("sizeof(p) = %zu, sizeof(*p) = %zu\n", sizeof(p), sizeof(*p));
	printf("sizeof(ptr) = %zu, sizeof(*ptr) = %zu\n", sizeof(ptr), sizeof(*ptr));
	p++;
	ptr++;

	printf("p = %p, ptr = %p\n", p, ptr);
	ana_are_mere(&arr);
	ana_are_pere(arr);

	return 0;
}

