#include <stdio.h>

#define container_of(ptr, type, member) ({			\
	const typeof(((type *)0)->member) * __mptr = (ptr);	\
	(type *)((char *)__mptr - offsetof(type, member)); })

int add(int a, int b)
{
	return a+b;
}

typedef int(*op_ptr)(int,int);
#define op_ptr_define(name) int(*name)(int,int)

int multiply(int a, int b)
{
	return a*b;
}

int division(int a, int b)
{
	return a/b;
}

typedef int (*(*get_multiply_ptr)(int(*f1)(int,int), int(*f2)(int,int)))(int,int);
/* op_ptr get_multiply(op_ptr f1, op_ptr f2) */
int (*get_multiply(int(*f1)(int,int), int(*f2)(int,int)))(int,int)
{
	if(f1 == NULL)
	{
		return multiply;
	}
	else
	{
		return division;
	}
}


void float_problem()
{
	float sum = 0;
	int i = 0;
	for (i = 0; i < 1000000; i++)
	{
		sum += 0.1;
	}
	printf("%f\n", sum);
	printf("%.24f\n", 0.1);
	sum = 1;
	if (sum == 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1)
	{
		printf("ana are mere\n");
	}
	else
	{
		printf("ana n-are mere\n");
	}
}


void double_conversion()
{
	double a = 1.2;
	printf("%d\n", (int)a);
	printf("0x%llx\n", *(long long unsigned*)&a);
}

int main()
{
	double_conversion();
	float_problem();
	op_ptr f = add;
	printf("%d\n", f(2,3));
	op_ptr op_mult = get_multiply(NULL, NULL);
	op_ptr op_div = get_multiply(f, NULL);
	printf("%d \n", op_mult(22,3));
	printf("%d \n", op_div(20,3));
	printf("%d \n", (int) sizeof(op_ptr));
	printf("%d \n", (int) sizeof (op_ptr_define()));
}

