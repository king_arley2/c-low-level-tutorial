#include <stdio.h>
#include <ctype.h>
#include <string.h>

typedef unsigned long long QWORD;
typedef unsigned int DWORD;
typedef unsigned short WORD;
typedef unsigned char BYTE;

void dumpBuffer(BYTE *buffer, int length, int nr_bytes_displayed)
{
    int i;
    int j;
    int buf_index;
    char displayBuffer[100];
    char tempBuffer[10];
    for (i = 0; i < length; i += nr_bytes_displayed)
    {
        displayBuffer[0] = '\0';
        for (j = 0; j < nr_bytes_displayed; j++)
        {
            buf_index = i + j;
            if (buf_index >= length)
            {
                break;
            }
            snprintf(tempBuffer, sizeof(displayBuffer), "%02X ", (unsigned char) buffer[buf_index]);
            strcat(displayBuffer, tempBuffer);
        }

        printf("0x%p: ", &buffer[i]);
        printf("%-*s| ", nr_bytes_displayed * 3, displayBuffer);

        for (j = 0; j < nr_bytes_displayed; j++)
        {
            buf_index = i + j;
            if (buf_index >= length)
            {
                break;
            }
            if (isprint(buffer[buf_index]))
            {
                printf("%c", buffer[buf_index]);
            }
            else
            {
                printf(".");
            }
        }

        printf("\n");
    }
    /* add another newline at the end of the dump for output clarity */
    printf("\n");
}

#define string_length(str) (sizeof(str) - 1)



void endianness()
{
	DWORD a = 0x636261;
	BYTE b[] = {0x12, 0x13, 0x14, 0x15};
	char *ana = "abcd";
	char ana2[] = "abcd";
	DWORD c = 0x68;
	DWORD d = 0x1234;

	dumpBuffer((BYTE *)&a, 4, 8);
	dumpBuffer((BYTE *)&b, 4, 8);
	dumpBuffer((BYTE *)&c, 4, 8);
	dumpBuffer((BYTE *)&d, 4, 8);
	dumpBuffer((BYTE *)ana, 5, 8);
	printf("sizeof ana: %lu\n", sizeof(ana));
	printf("sizeof ana2: %lu\n", sizeof(ana2));
	printf("b: 0x%x\n", *(WORD*)(&b));
	printf("(WORD)a :0x%x\n", (WORD)a);
	printf("*(WORD*)a :0x%x\n", *(WORD*)(&a));
}

typedef struct _A
{
	char a;
	QWORD b;
	char c;
	DWORD d;
	char e;
} A;

typedef struct _B
{
	char a;
	char c;
	char e;
	QWORD b;
	DWORD d;
} B;

#define offsetof(type, field) (DWORD)(size_t)(&(((type*)(NULL))->field))

int main()
{
	A a;
	B b;
	printf("%lu\n", sizeof(a));
	printf("%u %u %u %u %u\n", offsetof(A,a), offsetof(A,b), offsetof(A,c), offsetof(A,d), offsetof(A,e));
	printf("%lu\n", sizeof(b));
	printf("%u %u %u %u %u\n", offsetof(B,a), offsetof(B,c), offsetof(B,e), offsetof(B,b), offsetof(B,d));
}

